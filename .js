someVariable = {
  someProperty: {
    somethingElse: 2
  }
};

console.log(someVariable.someProperty.somethingElse); // Output: 42
//          ^ variable   ^ property   ^ property                 ^ value